require_relative('include')
require_disloku('git/Repository')
require_disloku('SysCmd')
#require('fileutils')

describe "GitRepository" do

	tempDir = File.join(File.dirname(__FILE__), "_tmp")
	gitDir = File.join(tempDir, "git-tmp")
	repo = nil

	before(:all) do
		FileUtils.mkpath(gitDir)
		Dir.chdir(gitDir)
		Disloku::SysCmd.new("git init").execute()

		File.write(File.join(gitDir, "first.txt"), 'Some glorious content')
		File.write(File.join(gitDir, "third.txt"), 'Third file that will be deleted')

		Disloku::SysCmd.new("git add --all").execute()
		Disloku::SysCmd.new("git commit -m \"first\"").execute()
		Disloku::SysCmd.new("git checkout -b tests").execute()

		repo = Disloku::Git::Repository.new(".")
	end

	it "should have a temp Git directory" do
		expect(Dir.exists?(gitDir)).to(eq(true))
	end

	it "should have an empty changeset for a clean repo" do
		changeSets = repo.getChangeSets()
		expect(changeSets.count).to(eq(1))
		expect(changeSets[0].count).to(eq(0))
	end

	it "should have a new file if one is added" do
		File.write(File.join(gitDir, "second.txt"), 'More glorious content')
		Disloku::SysCmd.new("git add --all").execute()

		changeSets = repo.getChangeSets()
		expect(changeSets.count).to(eq(1))
		expect(changeSets[0].count).to(eq(1))
		expect(changeSets[0].first().changeType).to(eq(:added))

		Disloku::SysCmd.new("git commit -m \"second\"").execute()
	end

	it "should have a modification if a file is changed" do
		File.write(File.join(gitDir, "first.txt"), 'Modified content')
		Disloku::SysCmd.new("git add --all").execute()

		changeSets = repo.getChangeSets()
		expect(changeSets.count).to(eq(1))
		expect(changeSets[0].count).to(eq(1))
		expect(changeSets[0].first().changeType).to(eq(:modified))

		Disloku::SysCmd.new("git commit -m \"second\"").execute()
	end

	it "should have a delete if a file is deleted" do
		File.delete(File.join(gitDir, "third.txt"))
		Disloku::SysCmd.new("git add --all").execute()

		changeSets = repo.getChangeSets()
		expect(changeSets.count).to(eq(1))
		expect(changeSets[0].count).to(eq(1))
		expect(changeSets[0].first().changeType).to(eq(:deleted))

		Disloku::SysCmd.new("git commit -m \"second\"").execute()
	end

	it "should list all cummulative changes from initial commit" do
		changeSets = repo.getChangeSets('master')
		expect(changeSets.count).to(eq(1))
		expect(changeSets[0].count).to(eq(3))
	end

	after(:all) do
		Dir.chdir(File.dirname(__FILE__))
		FileUtils.rm_r(tempDir, :force => true)
	end
end
