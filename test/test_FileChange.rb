require_relative('include')
require_disloku('FileChange')
require_disloku('util/File')

module Disloku

	describe "FileChange" do
		let(:repository) { double("repository") }

		it "should assign the repository" do
			expect(FileChange.new(repository, "", :type, false).repository).to(be(repository))
		end

		it "should assign the change type" do
			expect(FileChange.new(repository, "", :type, false).changeType).to(be(:type))
		end

		it "should assign the dirty flag" do
			expect(FileChange.new(repository, "", :type, false).dirty).to(eq(false))
			expect(FileChange.new(repository, "", :type, true).dirty).to(eq(true))
		end

		it "should create a file for a target" do
			change = FileChange.new(repository, "/a/b/c.x", :type, false)
			expect(repository).to receive(:root).and_return("/a")
			target = double("target")
			expect(target).to receive(:mapPath).and_return(nil)
			file = change.getFile(target)
			expect(file).to(be_a_kind_of(Util::File))
		end
	end

end
