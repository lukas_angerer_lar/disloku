require_relative('include')
require_disloku('config/YamlConfig')
require_disloku('config/Mapping')

describe "Mapping" do

	sampleOne = { "mapping" => [
			{ "src" => "root/one", "dst" => "mapped/alpha" },
			{ "src" => "root/two", "dst" => "mapped/beta" },
		] }

	shared_examples_for "sampleOne" do
		it "should not map unmapped paths" do
			expect(mapping.mapPath(["any"])).to(eq(nil))
			expect(mapping.mapPath([])).to(eq(nil))
			expect(mapping.mapPath(["some", "path"])).to(eq(nil))
			expect(mapping.mapPath(["root"])).to(eq(nil))
			expect(mapping.mapPath(["root", "something"])).to(eq(nil))
		end

		it "should map mapped paths" do
			expect(mapping.mapPath(["root", "one"])).to(eq(["mapped", "alpha"]))
			expect(mapping.mapPath(["root", "two", "x"])).to(eq(["mapped", "beta", "x"]))
			expect(mapping.mapPath(["root", "two", "x", "y"])).to(eq(["mapped", "beta", "x", "y"]))
		end
	end

	sampleTwo = { "baseMapping" => "base", "mapping" => [
			{ "src" => "root/one", "dst" => :block },
			{ "src" => "root/three", "dst" => "mapped/gamma" },
		] }

	shared_examples_for "sampleTwo" do
		it "should not map unmapped paths" do
			expect(mapping.mapPath(["any"])).to(eq(nil))
			expect(mapping.mapPath([])).to(eq(nil))
			expect(mapping.mapPath(["some", "path"])).to(eq(nil))
			expect(mapping.mapPath(["root", "one"])).to(eq(nil))
			expect(mapping.mapPath(["root", "one", "sub"])).to(eq(nil))
			expect(mapping.mapPath(["root", "something"])).to(eq(nil))
		end

		it "should map mapped paths" do
			expect(mapping.mapPath(["root", "two", "x"])).to(eq(["mapped", "beta", "x"]))
			expect(mapping.mapPath(["root", "two", "x", "y"])).to(eq(["mapped", "beta", "x", "y"]))
			expect(mapping.mapPath(["root", "three", "sub"])).to(eq(["mapped", "gamma", "sub"]))
		end
	end

	describe "Empty mapping" do
		mapping = Disloku::Config::Mapping.new(Disloku::Config::YamlConfig.new({}, false), nil, true)

		it "should map all paths as-is" do
			samples = [["a"], ["a", "b"]]
			samples.each() do |s|
				expect(mapping.mapPath(s)).to(eq(s))
			end
		end
	end

	describe "Simple mapping" do
		mapping = Disloku::Config::Mapping.new(Disloku::Config::YamlConfig.new(sampleOne, false), nil, true)

		it_behaves_like "sampleOne" do
			let(:mapping) { mapping }
		end
	end

	describe "Inherited mapping" do
		base = Disloku::Config::Mapping.new(Disloku::Config::YamlConfig.new(sampleOne, false), nil, true)

		it "fetches the base mapping from the store" do
			store = double("store")
			expect(store).to receive(:get) { "base" }.and_return(base)
			mapping = Disloku::Config::Mapping.new(Disloku::Config::YamlConfig.new(sampleTwo, false), store, true)
		end

		it_behaves_like "sampleTwo" do
			let(:mapping) do
				store = double("store")
				expect(store).to receive(:get) { "base" }.and_return(base)
				Disloku::Config::Mapping.new(Disloku::Config::YamlConfig.new(sampleTwo, false), store, true)
			end
		end
	end
end
