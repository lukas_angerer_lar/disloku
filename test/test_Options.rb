require_relative('include')
require_disloku('config/Options')
require_disloku('config/YamlConfig')

module Disloku; module Config

describe "Options" do

	it "should have defaults if no overrides are given" do
		config = Options.new(YamlConfig.new({ "packageDir" => "" }, false), {})
		expect(config.ignoreDeleteErrors).to(eq(false))
	end

	it "should throw an exception when the requested option does not exist" do
		config = Options.new(YamlConfig.new({ "packageDir" => "" }, false), {})
		expect() { config.test }.to(raise_error(ArgumentError))
	end

	it "config should override defaults" do
		config = Options.new(YamlConfig.new({ "packageDir" => "" , "ignoreDeleteErrors" => true }, false), {})
		expect(config.ignoreDeleteErrors).to(eq(true))
	end

	it "CLI options should override defaults" do
		config = Options.new(YamlConfig.new({ "packageDir" => "" }, false), { "ignoreDeleteErrors" => true })
		expect(config.ignoreDeleteErrors).to(eq(true))
	end

	it "CLI options should override config" do
		config = Options.new(YamlConfig.new({ "packageDir" => "", "ignoreDeleteErrors" => true }, false), { "ignoreDeleteErrors" => 42 })
		expect(config.ignoreDeleteErrors).to(eq(42))
	end

end

end; end
