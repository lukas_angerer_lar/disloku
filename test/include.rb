$dislokuDir = File.expand_path(File.join(File.dirname(__FILE__), "../lib/disloku"))

def require_disloku(file)
	require(File.join($dislokuDir, file))
end

require_disloku('Log')

Disloku::Log.instance.level(:default, Logger::ERROR)
