require_relative('include')
require_disloku('config/YamlConfig')

module Disloku; module Config

describe "Config" do

	configFile = File.dirname(__FILE__) + "/data/dummy.config"

	it "should load YAML file" do
		config = YamlConfig.new(configFile)
		expect(config.value()).not_to(eq(nil))
	end

	it "should load hash" do
		hash = { "a" => "aa", "b" => "bb"}
		config = Config::YamlConfig.new(hash, false)
		expect(config.value()).to(eq(hash))
	end

	it "should be able to retrieve single step keys" do
		config = Config::YamlConfig.new(configFile)
		expect(config["psftp"].value()).not_to(be_nil())
	end

	it "should be able to retrieve multipart keys" do
		config = Config::YamlConfig.new(configFile)
		expect(config["psftp.path"].value()).to(eq("C:\\some\\path"))
	end

	it "should be equivalent to retrieve multipart keys and multiple single step keys" do
		config = Config::YamlConfig.new(configFile)
		expect(config["psftp.path"].value()).to(eq(config["psftp"]["path"].value()))
	end

	it "should return an array of Config objects when getting the value of an array" do
		config = Config::YamlConfig.new(configFile)

		arrayConfig = config["psftp.target"].value()
		expect(arrayConfig).to(be_instance_of(Array))
		arrayConfig.each() do |element|
			expect(element).to(be_instance_of(YamlConfig))
		end
	end
end

end; end
