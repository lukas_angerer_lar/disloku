require_relative('include')
require_disloku('util/File')
require_disloku('DislokuError')

module Disloku; module Util

	describe "File" do

		context "Basics" do
			let(:target) { double("target") }
			let(:change) { double("change") }
			let(:file) { File.new("/some/file/path", "/some", target, change) }

			it "can be instantiated" do
				expect(target).to(receive(:mapPath))
				expect(file.change).to(be(change))
				expect(file.srcPath).to(eq("/some/file/path"))
			end

			it "has no mapping if target does not map" do
				expect(target).to(receive(:mapPath))
				expect(file.hasMapping?).to(eq(false))
			end

			it "has a mapping if target maps" do
				expect(target).to(receive(:mapPath).and_return([]))
				expect(file.hasMapping?).to(eq(true))
			end

			it "raises error if file is not in base" do
				expect() { File.new("/some/file/path", "/different/base", target, change) }.to(raise_error(Disloku::DislokuError))
				expect() { File.new("/some/path", "/some/path/deeper", target, change) }.to(raise_error(Disloku::DislokuError))
				expect() { File.new("/some/file/path", "/some/mismatched", target, change) }.to(raise_error(Disloku::DislokuError))
			end
		end

		context "Mapping" do
			let(:target) { double("target") }
			let(:change) { double("change") }

			it "keeps relative path" do
				expect(target).to(receive(:mapPath).with(["some", "file", "path"]).and_return(["some", "file", "path"]))
				file = File.new("/base/some/file/path", "/base", target, change)
				expect(file.getAbsoluteDstPath("/")).to(eq("/some/file/path"))
			end

			it "uses mapped path" do
				expect(target).to(receive(:mapPath).with(["some", "file", "path"]).and_return(["a", "b"]))
				file = File.new("/base/some/file/path", "/base", target, change)
				expect(file.getAbsoluteDstPath("/")).to(eq("/a/b"))
			end
		end
	end

end; end
