require_relative('include')
require_disloku('ChangeSet')

describe "ChangeSet" do
	it "should be empty when newly created" do
		expect(Disloku::ChangeSet.new().count).to(eq(0))
	end

	it "should contain one element after adding one" do
		changeSet = Disloku::ChangeSet.new()
		changeSet << 42
		expect(changeSet.count).to(eq(1))
	end

	it "should contain 'x' element after adding 'x'" do
		changeSet = Disloku::ChangeSet.new()
		changeSet << "x"
		expect(changeSet.first).to(eq("x"))
	end
end
