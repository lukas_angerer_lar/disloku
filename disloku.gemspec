specPath = File.dirname(__FILE__)
require('date')
require(File.join(specPath, 'lib/disloku/Constants'))

head = %x[git rev-parse --short=10 HEAD].strip()

Gem::Specification.new do |s|
  s.name        = 'disloku'
  s.version     = Disloku::VERSION
  s.date        = Date.today.strftime("%Y-%m-%d")
  s.summary     = "Disloku deployment tool"
  s.description = "A deployment tool that allows copying of Git changesets via sftp (version #{Disloku::VERSION} [#{head}])"
  s.authors     = ["Lukas Angerer"]
  s.email       = ["executor.dev@gmail.com"]
  s.homepage    = "https://rubygems.org/gems/disloku"
  s.files       = Dir.glob("lib/**/*") + ["LICENSE", "config/sample.config"]
  s.executables << 'disloku'
  s.license     = 'MIT'
  s.add_runtime_dependency "thor", ["~> 0.18"]
  s.add_runtime_dependency "net-sftp", ["~> 2.1"]
  s.add_runtime_dependency "rainbow", ["~> 2.0"]
  s.add_development_dependency "rake", ["~> 10.1"]
  s.add_development_dependency "rspec", ["~> 2.14"]
end
