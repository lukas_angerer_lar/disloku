
# Introduction
Disloku is a deployment utility for the kind of project where a _normal_ git deployment or something similar is simply not possible (for whatever reasons). It was developed with a specific deployment scenario in mind, but it should be able to adapt to a fairly broad range of situations.

The core idea is simple: Disloku operates on a Git repository and bundles all in the current working tree that have been modified since a given commit. It can then take this bundle of files and directly deploy them over SFTP to one or more targets. Through the Disloku configuration it is possible to specify a mapping from your local Git folder structure to a different folder structure on the target system(s).

# Installation
Disloku is available as a Ruby Gem, simply install it with

```
gem install disloku
```

# Updating
If there is a newer version of disloku available, you can update it like any other gem with

```
gem update disloku
```

# Getting Started
Start by changing to the root of the Git repository you would like to use and execute
```
disloku generate
```
This creates a new `disloku.config` file in your Git repository. Open this file with your favorite text editor and use the following template (or the generated config) to start defining your deployment target.

```
options:
  ignoreDeleteErrors: true
targets:
  development:
    connection:
      host: sample.org
      user: example
      password: drowssap
    targetDir: "/path/to/target/directory"
    mapping:
      -
        src: :any
        dst: :keep
  default:
    targets: [development]
```

Most of the configuration should be fairly self-explanatory. You can find more information on the configuration on the [Wiki](/lukas_angerer/disloku/wiki/Home).

Now, make some changes to the files in your Git repository and add them to the index with `git add --all`. With the basic configuration, you can now run
```
disloku build -o
```
to create a deployment package for the _default_ target. Once the package has been built, it will open the packaging folder (`~/disloku/`) and you can examine the (mapped) files that have been prepared.

Once you're confident that your mapping and connection configuration is correct, you can deploy your changes with
```
disloku deploy
```

# Documentation
You can find the full documentation on the [Wiki](/lukas_angerer/disloku/wiki/Home)!
