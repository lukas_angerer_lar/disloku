require_relative('Log')
require('net/sftp')
require('digest/sha1')

module Disloku
	class SessionManager

		include Singleton

		def initialize()
			@sessions = {}

			at_exit { shutdown!() }
		end

		def get(connection, &block)
			connection = getConnection(connection)
			block.call(connection)
		end

		def getConnection(connection)
			if (!@sessions.has_key?(connection.hash))
				Log.instance.info("creating new session #{connection.user}@#{connection.host} -> #{connection.hash}")
				session = Net::SSH.start(connection.host, connection.user, connection.options)
				sftp = Net::SFTP::Session.new(session).connect!
				@sessions[connection.hash] = { :sftp => sftp, :ssh => session }
			end

			return @sessions[connection.hash][:sftp]
		end

		def shutdown!()
			Log.instance.info("closing all open sessions")
			@sessions.each {|key, value| value[:ssh].close() }
		end

	end
end
