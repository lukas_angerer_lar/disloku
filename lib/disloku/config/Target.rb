require_relative('Mapping')
require_relative('Connection')

module Disloku
	module Config

		class Target
			attr_accessor :name, :connection

			def initialize(name, targetConfig, mappingStore, connectionStore)
				@name = name
				@config = targetConfig

				if (@config["connection"].value().kind_of?(String))
					@connection = connectionStore.get(@config["connection"].value())
				else
					@connection = Connection.new(@config["connection"])
				end

				@mapping = Mapping.new(@config, mappingStore)
			end

			def mapPath(pathSegments)
				return @mapping.mapPath(pathSegments)
			end

			def method_missing(name, *args, &block)
				if (!@config.has?(name.to_s()))
					return nil
				end

				return @config[name.to_s()].value()
			end
		end

	end
end
