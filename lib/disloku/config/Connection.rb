require('net/sftp')
require('digest/sha1')

module Disloku
	module Config

		class Connection
			attr_accessor :hash, :host, :user, :options

			def initialize(config)
				@host = config["host"].value()
				@user = config["user"].value() if !config["user"].nil?
				@options = {}
				addOption(config, :password)
				addOption(config, :port)
				addOption(config, :keys, true)

				@hash = Digest::SHA1.hexdigest([@host, @user, @options].join())
			end

			def addOption(config, key, unwrap = false)
				value = config[key.to_s()]
				if (!value.nil?)
					if (unwrap)
						@options[key] = value.value().map() { |e| e.value() }
					else
						@options[key] = value.value()
					end
				end
			end
		end

	end
end
