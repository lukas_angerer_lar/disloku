require_relative('../util/Hash')
require_relative('../util/File')

module Disloku
	module Config

		class Mapping

			def initialize(config, mappingStore = nil, allowDefault = true)
				@mapping = {}

				symbolize = Proc.new() do |s|
					match = s.match(/^:(.*)/)
					match.nil? ? s : match[1].to_sym()
				end

				mappingConfig = config["mapping"]
				if (!mappingConfig.nil?)
					mappingConfig.value().each() do |m|
						node = @mapping
						src = m["src"].value()

						if (src.kind_of?(Symbol))
							segments = [src]
						else
							segments = Util::File.getSegments(src).map(&symbolize)
						end

						segments[0..-2].each() do |segment|
							if (!node.has_key?(segment))
								node[segment] = {}
							end
							node = node[segment]
						end

						dst = m["dst"].value()
						if (dst.kind_of?(Symbol))
							node[segments[-1]] = dst
						else
							node[segments[-1]] = Util::File.getSegments(dst)
						end
					end
				elsif (allowDefault)
					@mapping[:any] = :keep
				end

				baseMapping = config["baseMapping"].nil? ? nil : config["baseMapping"].value()

				if (!baseMapping.nil?)
					if (mappingStore.nil?)
						raise ArgumentError.new("mapping has a base but no mapping manager was passed")
					else
						@mapping = mappingStore.get(baseMapping).getTree().recursive_merge(@mapping)
					end
				end
			end

			def getTree()
				return @mapping
			end

			def mapPath(pathSegments)
				node = @mapping
				for i in 0..pathSegments.count
					if (node.has_key?(pathSegments[i]))
						node = node[pathSegments[i]]
					elsif (node.has_key?(:any))
						node = node[:any]
					else
						return nil
					end

					if (node == :block)
						return nil
					elsif (node == :keep)
						return pathSegments
					elsif (node.kind_of?(Array))
						return Array.new(node).concat(pathSegments[(i + 1)..-1])
					end
				end
			end
		end

	end
end
