require_relative('../Log')
require_relative('../OsCommands')
require_relative('../SysCmd')
require('tmpdir')
require('fileutils')

module Disloku
	module Config

		class Options

			def initialize(config, cliOptions)
				@options = {
					:ignoreDeleteErrors => false,
					:createDeletesFile => false,
					:allowOverride => false,
					:target => "default",
					:packageDir => :temp,
					:inputDefault => :none,
					:assumeYes => false,
					:assumeVeryYes => false,
					:openPackageDir => true,
					:openDirCmd => :openDir,
				}

				@options.each_key() do |key|
					if (cliOptions.has_key?(key.to_s()))
						@options[key] = cliOptions[key.to_s()]
					elsif (config[key.to_s()] != nil)
						@options[key] = config[key.to_s()].value()
					end
				end

				processPackageDir()
				processInputDefaults()
			end

			def processPackageDir()
				if (@options[:packageDir] == :temp)
					@options[:packageDir] = Dir.mktmpdir("disloku")
					Log.instance.debug("Creating tmp directory #{@options[:packageDir]}")

					# make sure the temp directory is deleted when the program exists
					at_exit {
						Log.instance.debug("Removing tmp directory #{@options[:packageDir]}")
						FileUtils.rm_r(@options[:packageDir], :force => true)
					}
				else
					@options[:packageDir] = File.expand_path(@options[:packageDir])
				end
			end

			def processInputDefaults()
				if (@options[:assumeVeryYes] == true)
					@options[:inputDefault] = :veryYes
				elsif (@options[:assumeYes] == true)
					@options[:inputDefault] = :yes
				end
			end

			def method_missing(name, *args, &block)
				if (!@options.has_key?(name))
					raise ArgumentError.new("There's no option '#{name}' here")
				end

				return @options[name]
			end

			def getCommand(cmdKey, *args)
				cmd = @options[cmdKey]
				return cmd.kind_of?(Symbol) ? OsCommands.get(cmd, *args) : SysCmd.new(cmd, *args)
			end

		end

	end
end
