require_relative('../DislokuError')
require_relative('Connection')

module Disloku
	module Config
		class NamedConfigStore

			def initialize(config = nil)
				@store = {}
				if (!config.nil?)
					load(config)
				end
			end

			def get(name)
				if (@store.has_key?(name))
					return @store[name]
				else
					raise DislokuError.new("There is no stored object with the name '#{name}' in this store")
				end
			end

			def add(name, yamlConfig)
				@store[name] = transformConfig(yamlConfig)
			end

			def load(config)
				if (!config.nil?)
					config.value().each_key() do |key|
						add(key, config[key])
					end
				end
			end

			def transformConfig(yamlConfig)
				return yamlConfig
			end
		end

	end
end
