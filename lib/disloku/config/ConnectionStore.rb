require_relative('NamedConfigStore')
require_relative('Connection')

module Disloku
	module Config

		class ConnectionStore < NamedConfigStore

			def initialize(config = nil)
				super(config)
			end

			def transformConfig(yamlConfig)
				return Connection.new(yamlConfig)
			end
		end

		end
end
