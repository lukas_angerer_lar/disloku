require_relative('NamedConfigStore')
require_relative('Mapping')

module Disloku
	module Config

		class MappingStore < NamedConfigStore

			def initialize(config = nil)
				super(config)
			end

			def transformConfig(yamlConfig)
				return Mapping.new(yamlConfig, self, false)
			end
		end

	end
end
