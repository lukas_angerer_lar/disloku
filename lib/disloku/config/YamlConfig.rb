require('yaml')

module Disloku
	module Config

		class YamlConfig
			attr_accessor :yaml

			def initialize(config, isFile = true)
				if (isFile)
					@yaml = YAML.load_file(config)
				else
					@yaml = config
				end
			end

			def merge(base)
				@yaml = base.yaml.recursive_merge(@yaml)
			end

			def has?(key)
				return self[key] != nil
			end

			def get(keys)
				if (keys.empty?())
					return self
				end
				current = keys.shift()
				return @yaml.has_key?(current) ? YamlConfig.new(@yaml[current], false).get(keys) : nil
			end

			def [](key)
				return self.get(key.split('.'))
			end

			def value()
				if (@yaml.kind_of?(Array))
					return @yaml.map() { |item| YamlConfig.new(item, false) }
				end
				return @yaml
			end

			def to_s()
				return value().to_s()
			end

			def to_yaml()
				return @yaml.to_yaml()
			end
		end

	end
end
