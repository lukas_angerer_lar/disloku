require_relative('../DislokuError')
require_relative('../Log')
require_relative('../BaseTask')
require_relative('../CliAdapter')
require_relative('../SessionManager')
require_relative('../config/Options')
require_relative('../config/Target')

module Net; module SFTP; module Operations
	class Upload
		alias old_on_mkdir on_mkdir
		def on_mkdir(response)
			begin
				old_on_mkdir(response)
			rescue
				if (@options[:ignoreMkdirError])
					process_next_entry
				else
					raise
				end
			end
		end
	end
end; end; end

module Disloku
	module Tasks
		class NetSftpTask < BaseTask

			def initialize(input)
				super()
				@repository = getInputParam(input, :repository, Disloku::Repository)
				@options = getInputParam(input, :options, Config::Options)
				@directory = getInputParam(input, :directory, String)
				@files = getInputParam(input, :files, Array)
				@target = getInputParam(input, :target, Config::Target)
				@dirty = getInputParam(input, :dirty, Object)
			end

			def beforeExecute()
				if (@files.count == 0)
					CliAdapter.puts("Nothing to deploy for target [#{@target.name}]")
					return false
				end

				if (!@target.branchLock.nil?)
					branch = @repository.getBranchName()
					if (branch != @target.branchLock)
						raise DislokuError.new("Target [#{@target.name}] is locked to branch #{@target.branchLock} but current branch is #{branch}", true)
					end
				end

				if (@dirty && !@target.allowDirty.nil? && !@target.allowDirty)
					raise DislokuError.new("Target does not allow deployments from dirty repositories", true)
				end

				CliAdapter.puts()
				CliAdapter.puts("Target [#{@target.name}]: #{@target.user}@#{@target.host}:#{@target.targetDir}")
				@files.each() do |file|
					puts(file)
				end

				return CliAdapter.queryYesNo("Continue with deployment?")
			end

			def executeTask()
				CliAdapter.puts("Doploying target [#{@target.name}]")

				Log.instance.scope([:default, :logfile]) do

					SessionManager.instance.get(@target.connection) do |sftp|
						Log.instance.info("connection with [#{@target.name}] established")

						sftp.upload!(@directory, @target.targetDir, { :ignoreMkdirError => true }) do |event, uploader, *args|
							case event
								when :open then
									# args[0] : file metadata
									# "starting upload: #{args[0].local} -> #{args[0].remote} (#{args[0].size} bytes}"
									CliAdapter.print(".")
									Log.instance.info("#{args[0].local} -> #{args[0].remote} (#{args[0].size} bytes)")
							end
						end
						CliAdapter.puts()

						@files.each() do |file|
							if (file.change.changeType == :deleted)
								path = file.getAbsoluteDstPath()
								Log.instance.info("deleting file #{path}")
								begin
									sftp.remove!(path)
									CliAdapter.print("x")
								rescue
									if (!@options.ignoreDeleteErrors)
										Log.instance.fatal("unable to delete file #{path} - failing")
										raise
									else
										Log.instance.warn("unable to delete file #{path} (it probably doesn't exist)")
									end
								end
							end
						end
					end

				end

			end

			def afterExecute()
			end
		end
	end
end
