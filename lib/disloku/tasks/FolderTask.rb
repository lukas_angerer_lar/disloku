require_relative('../Log')
require_relative('../BaseTask')
require_relative('../config/Options')
require_relative('../config/Target')
require('fileutils')
require('stringio')

module Disloku
	module Tasks
		class FolderTask < BaseTask

			def initialize(input)
				super()
				@options = getInputParam(input, :options, Config::Options)
				@changesets = getInputParam(input, :changesets, Array)
				@target = getInputParam(input, :target, Config::Target)
				@deletes = StringIO.new()

				@targetDirectory = File.join(@options.packageDir, @target.name)
			end

			def beforeExecute()
				if (!Dir.exists?(@targetDirectory))
					FileUtils.mkpath(@targetDirectory)
				elsif (Dir.exists?(@targetDirectory) and !@options.allowOverride)
					raise Exception.new("Directory '#{@targetDirectory}' already exists")
				elsif (Dir.exists?(@targetDirectory))
					FileUtils.rm_r(@targetDirectory, :force => true)
					Dir::mkdir(@targetDirectory)
				end

				@result[:directory] = @targetDirectory
				@result[:files] = []
			end

			def executeTask()
				@changesets.each() do |changeset|
					changeset.each(&method(:executeOnFileChange))
				end
			end

			def executeOnFileChange(change)
				file = change.getFile(@target)
				if (!file.hasMapping?())
					return
				end

				destination = file.getAbsoluteDstPath(@targetDirectory)

				case change.changeType
					when :modified, :added
						Log.instance.info("adding file #{file.srcPath}")
						if (!Dir.exists?(File.dirname(destination)))
							FileUtils.mkpath(File.dirname(destination))
						end
						FileUtils.cp(file.srcPath, destination)
					when :deleted
						Log.instance.info("adding file #{file.srcPath} to deletion list")
						addDelete(file.getAbsoluteDstPath())
					else
						Log.instance.warn("ignoring change type #{change.changeType}")
						return
				end

				@result[:files].push(file)
			end

			def afterExecute()
				if (@options.createDeletesFile)
					File.write(File.join(@targetDirectory, ".deletes"), @deletes.string)
				end
			end

			def addDelete(fullPath)
				@deletes << "#{fullPath}\n"
			end

		end
	end
end
