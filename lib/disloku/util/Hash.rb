
class Hash
	def recursive_merge(other)
		return merge(other) do |key, oldv, newv|
			if (oldv.kind_of?(Hash) && newv.kind_of?(Hash))
				oldv.recursive_merge(newv)
			else
				newv
			end
		end
	end
end
