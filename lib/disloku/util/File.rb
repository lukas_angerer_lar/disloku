require_relative('../DislokuError')

module Disloku
	module Util

		if (File::ALT_SEPARATOR.nil?)
			SPLIT_EXP = "\\#{File::SEPARATOR}"
		else
			SPLIT_EXP = "\\#{File::SEPARATOR}|\\#{File::ALT_SEPARATOR}"
		end

		class File

			def self.getSegments(path)
				return path.split(/#{SPLIT_EXP}/)
			end

			attr_accessor :srcPath, :change

			def initialize(filePath, basePath, target, change)
				@srcPath = filePath
				@target = target
				@change = change

				fileSegments = File.getSegments(filePath)
				baseSegments = File.getSegments(basePath)

				if (fileSegments.count < baseSegments.count)
					raise DislokuError.new("file path '#{filePath}' is shorter than base path '#{basePath}'")
				end

				index = 0
				while (index < baseSegments.count && fileSegments[index] == baseSegments[index])
					index += 1
				end

				if (index < baseSegments.count)
					raise DislokuError.new("file path '#{filePath}' is not in base path '#{basePath}'")
				end

				@relativeSrcSegments = fileSegments[index..-1]
				@relativeDstSegments = target.mapPath(@relativeSrcSegments)
			end

			def hasMapping?()
				return @relativeDstSegments != nil
			end

			def getAbsoluteDstPath(basePath = nil)
				basePath = basePath || @target.targetDir
				return ::File.join(basePath, *@relativeDstSegments)
			end

			def to_s()
				operation = (@change.changeType == :deleted ? "x>" : "->")
				return "#{srcPath} #{operation} #{getAbsoluteDstPath()}"
			end
		end
	end
end
