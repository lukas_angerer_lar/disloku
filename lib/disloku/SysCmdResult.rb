require_relative('Log');

module Disloku
	class SysCmdResult
		attr_accessor :output, :exitCode, :result
		def initialize(output, result)
			@output = output
			@result = result
			@exitCode = result.to_i()
		end
	end
end
