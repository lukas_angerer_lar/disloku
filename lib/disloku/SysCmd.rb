require_relative('Log');
require_relative('SysCmdResult');

module Disloku
	class SysCmd
		def initialize(cmd, *args)
			@cmd = cmd
			if (!args.empty?)
				@cmd = @cmd.gsub(/\$(\d+)/) { |m| args[m[1].to_i() - 1] }
			end
		end

		def execute()
			Log.instance.debug("executing '#{@cmd}'")
			return SysCmdResult.new(%x(#{@cmd}), $?)
		end
	end
end
