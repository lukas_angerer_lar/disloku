require_relative('SysCmd')
require('rbconfig')

module Disloku

	class OsCommands
		class << self

			@@os = (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/ ? :win : :unix)

			@@commands = {
				:openDir => {
					:win => "explorer \"$1\"",
					:unix => "xdg-open \"$1\"",
				}
			}

			def get(cmdKey, *args)
				return SysCmd.new(@@commands[cmdKey][@@os], *args)
			end

			def convertPath(path)
				return @@os == :win ? path.gsub(File::SEPARATOR, File::ALT_SEPARATOR) : path
			end
		end
	end

end
