
# TODO: start from scratch for SVN repository provider

# require_relative('../ChangeSetProvider');
# require_relative('../ChangeSet');
# require_relative('../FileChange');
# require_relative('../SysCmd');

# module Disloku
# 	module Svn

# 		CHANGE_MAP = {
# 			' ' => :nochange,
# 			'A' => :added,
# 			'C' => :conflicted,
# 			'D' => :deleted,
# 			'I' => :ignored,
# 			'M' => :modified,
# 			'R' => :replaced,
# 			'X' => :external,
# 			'?' => :unversioned,
# 			'!' => :missing,
# 			'~' => :obstructed,
# 		}

# 		class ChangeSetProvider < Disloku::ChangeSetProvider
# 			def getChangeSets(from, to)
# 				status = SysCmd.new("svn status #{repository.root}").execute()

# 				result = ChangeSet.new()
# 				status.output.each_line do |line|
# 					match = /^(.)......\s+(.*)$/.match(line)
# 					result << FileChange.new(repository, match[2], getChangeType(match[1]))
# 				end

# 				return [result]
# 			end

# 			def getChangeType(changeChar)
# 				return CHANGE_MAP[changeChar]
# 			end
# 		end
# 	end
# end
