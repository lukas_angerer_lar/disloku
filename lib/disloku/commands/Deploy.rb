require_relative('../DislokuError')
require_relative('../BaseCoreCommand')
require_relative('../CliAdapter')
require_relative('../Log')
require_relative('../tasks/FolderTask')
require_relative('../tasks/NetSftpTask')

module Disloku
	module Commands

		class Deploy < BaseCoreCommand
			def initialize(cliOptions)
				super(cliOptions)
			end

			def executeCommand(from)
				changesets = @repository.getChangeSets(from)
				dirty = changesets.any?() { |c| c.dirty?() }

				folderInput = {
					:options => @options,
					:changesets => changesets,
				}

				resolveTargets([@options.target]).each() do |t|
					begin
						folderInput[:target] = t

						result = Tasks::FolderTask.new(folderInput).execute()

						sftpInput = result.merge({
							:repository => @repository,
							:options => @options,
							:target => t,
							:dirty => dirty,
						})

						result = Tasks::NetSftpTask.new(sftpInput).execute()
					rescue DislokuError => e
						if (e.recoverable?)
							Log.instance.error(e.message)
							if (!CliAdapter.queryYesNo("Continue with next target?", true))
								return
							end
						else
							raise
						end
					end
				end
			end
		end

	end
end
