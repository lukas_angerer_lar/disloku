require_relative('../BaseCoreCommand')
require_relative('../tasks/FolderTask')
require_relative('../OsCommands')

module Disloku
	module Commands

		class Build < BaseCoreCommand
			def initialize(cliOptions)
				super(cliOptions)
			end

			def executeCommand(from)
				changesets = @repository.getChangeSets(from)

				folderInput = {
					:options => @options,
					:changesets => changesets,
					:target => nil,
				}

				resolveTargets([@options.target]).each() do |t|
					folderInput[:target] = t

					Tasks::FolderTask.new(folderInput).execute()
				end

				convertedPath = OsCommands.convertPath(@options.packageDir)
				Log.instance.info("Opening package directory #{convertedPath}")
				@options.getCommand(:openDirCmd, convertedPath).execute()
			end
		end

	end
end
