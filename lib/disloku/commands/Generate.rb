require_relative('../DislokuError')
require('fileutils')

module Disloku
	module Commands

		class Generate < BaseCommand
			def initialize(cliOptions)
				super(cliOptions)

				@repository = @scmImplementation.new(cliOptions[:dir])
			end

			def execute()
				repoConfig = File.join(@repository.root, 'disloku.config')
				if (File.exists?(repoConfig))
					raise DislokuError.new("disloku.config already exists in this repository")
				else
					sampleConfig = File.expand_path(File.join(File.dirname(__FILE__), "../../../config/sample.config"))
					FileUtils.cp(sampleConfig, repoConfig)
				end
			end
		end

	end
end
