require_relative('../BaseCoreCommand')

module Disloku
	module Commands

		class Config < BaseCoreCommand
			def initialize(cliOptions)
				super(cliOptions)
			end

			def executeCommand()
				puts(config.to_yaml())
			end
		end

	end
end
