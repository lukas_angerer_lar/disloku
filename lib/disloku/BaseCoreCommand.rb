require_relative('DislokuError')
require_relative('BaseCommand')
require_relative('Log')
require_relative('CliAdapter')
require_relative('config/YamlConfig')
require_relative('config/Options')
require_relative('config/MappingStore')
require_relative('config/ConnectionStore')
require_relative('config/Target')

module Disloku

	class BaseCoreCommand < BaseCommand
		attr_accessor :repository, :config, :options

		def initialize(cliOptions)
			super(cliOptions)

			@repository = @scmImplementation.new(cliOptions[:dir])
			@config = loadConfiguration()
			@options = Config::Options.new(@config["options"], cliOptions)
			@mappingStore = Config::MappingStore.new(@config["mappings"])
			@connectionStore = Config::ConnectionStore.new(@config["connections"])

			Log.instance.addLogTarget(:logfile, File.join(repository.root, "disloku.log"), Logger::INFO)
			CliAdapter.setYesNoBehavior(@options.inputDefault)
		end

		def loadConfiguration()
			repoConfig = File.join(repository.root, 'disloku.config')
			if (!File.exists?(repoConfig))
				raise DislokuError.new("There is no disloku.config file in #{repository.root}")
			end

			config = Config::YamlConfig.new(repoConfig)

			userHome = File.expand_path("~")
			userConfig = File.join(userHome, ".disloku.config")
			if (File.exists?(userConfig))
				base = Config::YamlConfig.new(userConfig)
				config.merge(base)
			end

			return config
		end

		def resolveTargets(targets)
			actualTargets = []

			while (targets.count > 0)
				current = targets.shift()
				targetConfig = @config["targets"][current]
				if (targetConfig != nil)
					if (targetConfig["targets"] != nil)
						targetConfig["targets"].value().each() { |x| targets.push(x.value()) }
						next
					end

					actualTargets.push(Config::Target.new(current, targetConfig, @mappingStore, @connectionStore))
				end
			end

			return actualTargets
		end

		def execute(*args)
			send(:executeCommand, *args)
		end
	end

end
