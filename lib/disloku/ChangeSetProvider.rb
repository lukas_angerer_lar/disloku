
module Disloku
	class ChangeSetProvider
		attr_accessor :repository

		def initialize(repository)
			@repository = repository
		end

		def getChangeSets(from, to)
			raise NotImplementedError.new()
		end
	end
end
