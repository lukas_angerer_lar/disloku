require_relative('Log')
require_relative('util/File')

module Disloku
	class Repository
		attr_accessor :location, :root

		def initialize(location)
			@location = location
			@root = getRepositoryRoot()
			@gitDir = File.join(@root, ".git")
			@provider = getProvider()
		end

		def getRepositoryRoot()
			raise NotImplementedError.new()
		end

		def getBranchName()
			raise NotImplementedError.new()
		end

		def getProvider()
			raise NotImplementedError.new()
		end

		def getChangeSets(from = nil, to = nil)
			changeSets = @provider.getChangeSets(from, to)

			Log.instance.scope([:default, :logfile]) do
				changeSets.each() do |changeSet|
					Log.instance.info("gathered change set #{changeSet.to_s()}")
				end
			end

			return changeSets
		end
	end
end
