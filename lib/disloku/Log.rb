require_relative('util/Kernel')
require('singleton')
require('logger')
require('rainbow')

module Disloku
	class Log

		include Singleton

		def initialize()
			@loggers = {}

			rainbow = Rainbow.new()
			rainbow.enabled = true
			colors = {
				"DEBUG" => [:green, :normal],
				"INFO" => [:cyan, :normal],
				"WARN" => [:yellow, :bright],
				"ERROR" => [:red, :normal],
				"FATAL" => [:red, :bright],
			}

			addLogTarget(:default, STDOUT, Logger::INFO) do |severity, datetime, progname, msg|
				line = rainbow.wrap("#{msg}")
				if (colors.has_key?(severity))
					c = colors[severity]
					line = line.color(c[0])
					if (c[1] == :bright)
						line = line.bright()
					end
				end
				line + "\n"
			end

			@scope = [[:default]]
		end

		def addLogTarget(key, io, level, &block)
			@loggers[key] = Logger.new(io)
			if (!block.nil?)
				@loggers[key].formatter = block
			end
		end

		def getLogTarget(key)
			return @loggers[key]
		end

		def scope(scopeTargets)
			if (scopeTargets.kind_of?(Symbol))
				scopeTargets = [scopeTargets]
			end

			@scope.push(scopeTargets)
			yield
			@scope.pop()
		end

		def level(key, level)
			@loggers[key].level = level
		end

		def debug(message)
			log(:debug, message)
		end

		def info(message)
			log(:info, message)
		end

		def warn(message)
			log(:warn, message)
		end

		def error(message)
			log(:error, message)
		end

		def fatal(message)
			log(:fatal, message)
		end

		def log(method, message)
			@scope.last().each() do |targetKey|
				@loggers[targetKey].send(method, message) if !@loggers[targetKey].nil?
			end
		end
	end
end
