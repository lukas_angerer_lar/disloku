
module Disloku
	class BaseTask
		attr_accessor :changesets

		def initialize()
			@result = {}
		end

		def getInputParam(input, name, klass)
			if (!input.has_key?(name))
				raise ArgumentError.new("Missing input argument '#{name}' of type '#{klass}'")
			end
			if (!input[name].kind_of?(klass))
				raise ArgumentError.new("Input argument '#{name}' is not of type '#{klass}'")
			end

			return input[name]
		end

		def execute()
			Log.instance.info("running task '#{self.class}'")
			if (beforeExecute() != false)
				executeTask()
				afterExecute()
			end
			return @result
		end

		def beforeExecute()
		end

		def executeTask()
			raise NotImplementedError.new()
		end

		def afterExecute()
		end
	end
end
