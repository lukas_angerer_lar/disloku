require('io/console')
require('rainbow')

module Disloku

	class CliAdapter

		class << self

			@@in = $stdin
			@@out = $stdout
			@@rainbow = Rainbow.new()
			@@rainbow.enabled = true
			@@default = :none

			def setYesNoBehavior(default)
				@@default = default
			end

			def queryYesNo(question, highPrio = false)
				color = highPrio ? :yellow : :green
				@@out.puts()
				@@out.puts(@@rainbow.wrap("#{question} (Y/N)?").color(color))

				if ((@@default == :veryYes) || (@@default == :yes && !highPrio))
					self.puts("Assuming 'yes'")
					return true
				else
					char = @@in.getch()
					return !char.match(/^[Yy]/).nil?
				end
			end

			def queryYesNo!(question, highPrio = false)
				if (queryYesNo(question, highPrio))
					yield
				end
			end

			def puts(output = nil)
				@@out.puts(output)
			end

			def print(output = nil)
				@@out.print(output)
			end

		end
	end

end
