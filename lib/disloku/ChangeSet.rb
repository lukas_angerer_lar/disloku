require('forwardable')
require('pathname')

module Disloku
	class ChangeSet
		include Enumerable
		extend Forwardable
		def_delegators :@changes, :each, :<<

		attr_accessor :idFrom, :idTo

		def initialize(idFrom = "", idTo = "")
			@changes = Array.new()
			@idFrom = idFrom
			@idTo = idTo
		end

		def dirty?()
			return @changes.any?() { |f| f.dirty }
		end

		def to_s()
			return "#{idFrom} -> #{idTo} (#{dirty? ? 'dirty' : 'clean'})"
		end
	end
end
