require_relative('../Repository')
require_relative('../SysCmd')
require_relative('ChangeSetProvider')

module Disloku
	module Git
		class Repository < Disloku::Repository
			def initialize(location)
				super(location)
			end

			def getRepositoryRoot()
				old = Dir.pwd()
				Dir.chdir(location)
				status = SysCmd.new("git rev-parse --show-toplevel").execute()
				Dir.chdir(old)

				return status.output.strip()
			end

			def getBranchName()
				return @branch || begin
					branch = SysCmd.new("git --git-dir=\"#{@gitDir}\" rev-parse --abbrev-ref HEAD").execute()
					@branch = branch.output.strip()
				end
			end

			def getHashOfRefspec(refspec)
				hash = SysCmd.new("git --git-dir=\"#{@gitDir}\" rev-parse #{refspec}").execute()
				return hash.output.strip()
			end

			def getHeadHash()
				return @headHash || begin
					headHash = SysCmd.new("git --git-dir=\"#{@gitDir}\" rev-parse HEAD").execute()
					@headHash = branch.output.strip()
				end
			end

			def getProvider()
				return ChangeSetProvider.new(self)
			end
		end
	end
end
