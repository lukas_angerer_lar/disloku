require_relative('../ChangeSetProvider');
require_relative('../ChangeSet');
require_relative('../FileChange');
require_relative('../SysCmd');

module Disloku
	module Git

		CHANGE_MAP = {
			' ' => :nochange,
			'A' => :added,
			'C' => :copied,
			'D' => :deleted,
			'M' => :modified,
			'R' => :renamed,
			'U' => :unmerged,
			'T' => :typechange,
			'X' => :unknown,
			'B' => :broken,
		}

		class ChangeSetProvider < Disloku::ChangeSetProvider
			def getChangeSets(from = nil, to = nil)
				if (from == nil)
					from = "HEAD"
				end

				dirtyPaths = {}
				status = SysCmd.new("git status --untracked-files=no --porcelain #{repository.root}").execute()
				status.output.each_line() do |line|
					match = /^(.)(.)\s+"([^"]+)"/.match(line) || /^(.)(.)\s+([^ \n\r]+)/.match(line)
					dirtyPaths[match[3]] = true
				end

				result = ChangeSet.new(@repository.getHashOfRefspec(from), to || @repository.getHashOfRefspec("HEAD"))

				if (to == nil)
					diff = SysCmd.new("git diff --name-status --staged #{from} #{repository.root}").execute()
				else
					# defining a 'to' means that this particular ref has to be checked out first
					raise NotImplementedError.new()
					#diff = SysCmd.new("git diff --name-status #{from} #{to} #{repository.root}").execute()
				end

				Log.instance.scope([:default, :logfile]) do
					diff.output.each_line() do |line|
						match = /^(.)\s+(.*)$/.match(line)
						dirty = dirtyPaths.has_key?(match[2])
						change = FileChange.new(repository, match[2], getChangeType(match[1]), dirty)
						Log.instance.info(change.to_s())
						result << change
					end
				end

				return [result]
			end

			def getChangeType(changeChar)
				return CHANGE_MAP[changeChar]
			end
		end
	end
end
