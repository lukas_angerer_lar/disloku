
module Disloku
	class DislokuError < StandardError

		def initialize(msg = nil, recoverable = false)
			super(msg)
			@recoverable = recoverable
		end

		def recoverable?()
			return @recoverable
		end

	end
end
