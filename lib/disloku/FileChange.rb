require_relative('util/File')

module Disloku
	class FileChange
		attr_accessor :repository, :changeType, :dirty

		def initialize(repository, fullPath, changeType, dirty)
			@repository = repository
			@fullPath = fullPath
			@changeType = changeType
			@dirty = dirty
		end

		def getFile(target)
			return Util::File.new(@fullPath, @repository.root, target, self)
		end

		def to_s()
			return "<#{@changeType}> #{@fullPath}" + (@dirty ? " (dirty)" : "")
		end
	end
end
