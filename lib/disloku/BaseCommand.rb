require_relative('Log')
require_relative('git/Repository')

module Disloku

	class BaseCommand
		attr_accessor :repository, :config, :options

		def initialize(cliOptions)
			processGlobalOptions(cliOptions)
		end

		def processGlobalOptions(cliOptions)
			if (cliOptions[:scm].nil? || cliOptions[:scm] == "git")
				@scmImplementation = Git::Repository
			end

			if (cliOptions[:debug])
				Log.instance.level(:default, Logger::DEBUG)
			elsif (cliOptions[:verbose])
				Log.instance.level(:default, Logger::INFO)
			else
				Log.instance.level(:default, Logger::WARN)
			end
		end
	end

end
