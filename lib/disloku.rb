require_relative('disloku/Log')
require_relative('disloku/Constants')
require_relative('disloku/commands/Config')
require_relative('disloku/commands/Build')
require_relative('disloku/commands/Deploy')
require_relative('disloku/commands/Generate')
require_relative('disloku/SysCmd')
require_relative('disloku/SessionManager')
require('thor')

class DislokuCli < Thor

	def self.run()
		begin
			DislokuCli.start()
		rescue Interrupt => e
			Disloku::Log.instance.warn("Disloku was killed with CTRL+C")
		rescue Exception => e
			Disloku::Log.instance.fatal("Unhandled exception: #{e.message}")
			if (ARGV.any?() { |a| a == "-d" || a == "--debug" })
				Disloku::Log.instance.fatal(e.backtrace.join("\n"))
			end
		end
	end

	class_option :verbose, :aliases => "-v", :type => :boolean, :desc => "verbose output"
	class_option :debug, :aliases => "-d", :type => :boolean, :desc => "debug output"
	class_option :scm, :aliases => "-s", :type => :string, :desc => "source control adapter to use (git) - defaults to git"
	class_option :dir, :default => ".", :aliases => "-d", :desc => "repository directory - defaults to current directory"
	class_option :assumeYes, :aliases => "-y", :type => :boolean, :desc => "assume 'yes' to all basic questions"
	class_option :assumeVeryYes, :aliases => "-Y", :type => :boolean, :desc => "assume 'yes' to _all_ questions"

	desc "deploy [FROM]", "deploy changes"
	method_option :target, :aliases => "-t", :desc => "target"
	method_option :packageDir, :aliases => "-p", :desc => "directory in which the deployment package will be created (a temp folder is created if this option is omitted)"
	method_option :ignoreDeleteErrors, :aliases => "--ignore-delete-errors", :type => :boolean, :desc => "ignore remote delete errors"
	def deploy(from = nil)
		cmd = Disloku::Commands::Deploy.new(options)
		cmd.execute(from)
	end

	desc "build [FROM]", "build change package"
	method_option :target, :aliases => "-t", :desc => "target"
	method_option :packageDir, :default => "~/disloku", :aliases => "-p", :desc => "directory in which the deployment package will be created (~/disloku if this option is omitted)"
	method_option :allowOverride, :aliases => "-o", :type => :boolean, :desc => "overwrites existing deployment packages in packageDir"
	method_option :createDeletesFile, :aliases => "--create-deletes-file", :type => :boolean, :desc => "creates a .deletes file containing a list of deleted files"
	method_option :openPackageDir, :type => :boolean, :desc => "open package directory after successfull creation"
	def build(from = nil)
		cmd = Disloku::Commands::Build.new(options)
		cmd.execute(from)
	end

	desc "generate", "creates a new sample disloku.config file in the target directory"
	def generate()
		cmd = Disloku::Commands::Generate.new(options)
		cmd.execute()
	end

	desc "config", "show configuration"
	def config()
		cmd = Disloku::Commands::Config.new(options)
		cmd.execute()
	end

	desc "version", "show version"
	def version()
		puts(Disloku::VERSION)
	end
end
